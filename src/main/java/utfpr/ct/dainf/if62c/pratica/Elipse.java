/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author root
 */
public class Elipse implements FiguraComEixos {
    private double r, s, a, p;
    
    public Elipse(){
        super();
        a=0;
        p=0;
    }
    
    public Elipse(double r, double s){        
        this.r = r;
        this.s = s;
    }
    
    public double getArea(){
        this.a = Math.PI * r * s;
        return a; 
    }
    
    public double getPerimetro(){
        this.p = Math.PI*( 3*(r+s)-Math.sqrt((3*r+s)*(r+3*s)));
        return p;
    }
    
    public double getEixoMenor(){
        return r;
    }
    
    public double getEixoMaior(){
        return s;
    }
}
