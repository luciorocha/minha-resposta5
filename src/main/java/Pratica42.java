
import utfpr.ct.dainf.if62c.pratica.Circulo;
import utfpr.ct.dainf.if62c.pratica.Elipse;

public class Pratica42 {

    public static void main(String[] args) {

        Circulo c = new Circulo(10);
        Elipse e = new Elipse(10, 20);
        System.out.println("Circulo (area): " + c.getArea());
        System.out.println("Circulo (perimetro): " + c.getPerimetro());
        System.out.println("Elipse (area): " + e.getArea());
        System.out.println("Elipse (perimetro): " + e.getPerimetro());
    }
}
